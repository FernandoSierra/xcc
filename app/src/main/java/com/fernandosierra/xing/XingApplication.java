package com.fernandosierra.xing;

import android.app.Application;

import com.fernandosierra.xing.data.db.DbHelper;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
public class XingApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DbHelper dbHelper = new DbHelper(this);
        dbHelper.getWritableDatabase();
    }
}
