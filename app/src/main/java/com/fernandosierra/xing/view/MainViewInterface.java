package com.fernandosierra.xing.view;

import android.content.ContentResolver;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
public interface MainViewInterface {
    int INITIAL_PAGE = 1;

    void showRefresh();

    void hideRefresh();

    ContentResolver getViewContentResolver();
}
