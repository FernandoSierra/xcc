package com.fernandosierra.xing.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fernandosierra.xing.R;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
class RepoHolder extends RecyclerView.ViewHolder {
    View root;
    TextView name;
    TextView description;
    TextView owner;

    RepoHolder(View itemView) {
        super(itemView);
        root = itemView;
        name = ((TextView) itemView.findViewById(R.id.text_name));
        description = ((TextView) itemView.findViewById(R.id.text_description));
        owner = ((TextView) itemView.findViewById(R.id.text_owner));
    }
}
