package com.fernandosierra.xing.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fernandosierra.xing.Constants;
import com.fernandosierra.xing.R;
import com.fernandosierra.xing.data.Repo;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */

public class UrlRepoDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    private Repo repo;

    public static UrlRepoDialog newInstance(Repo repo) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.EXTRA_REPO, repo);
        UrlRepoDialog dialog = new UrlRepoDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        repo = getArguments().getParcelable(Constants.EXTRA_REPO);
        View root = inflater.inflate(R.layout.url_repo_dialog, container, false);
        ((TextView) root.findViewById(R.id.text_title)).setText(repo.getName());
        root.findViewById(R.id.button_repo_url).setOnClickListener(this);
        root.findViewById(R.id.button_owner_url).setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = null;
        switch (v.getId()) {
            case R.id.button_repo_url:
                uri = Uri.parse(repo.getHtmlUrl());
                break;
            case R.id.button_owner_url:
                uri = Uri.parse(repo.getOwner().getHtmlUrl());
                break;
        }
        intent.setData(uri);
        startActivity(intent);
    }
}
