package com.fernandosierra.xing.view.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import static com.fernandosierra.xing.view.MainViewInterface.INITIAL_PAGE;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
public class EndlessScrollListener extends RecyclerView.OnScrollListener {
    private static final int THRESHOLD_LIMIT = 3;
    private RecyclerView.LayoutManager layoutManager;
    private int currentPage = INITIAL_PAGE;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private OnBottomReached onBottomReached;

    public EndlessScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public void setOnBottomReached(OnBottomReached onBottomReached) {
        this.onBottomReached = onBottomReached;
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();

        if (totalItemCount < previousTotalItemCount) {
            currentPage = INITIAL_PAGE;
            previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                loading = true;
            }
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        if (!loading && (lastVisibleItemPosition + THRESHOLD_LIMIT) > totalItemCount) {
            currentPage++;
            if (onBottomReached != null) {
                onBottomReached.loadPage(currentPage);
            }
            loading = true;
        }
    }

    public void reset() {
        currentPage = INITIAL_PAGE;
        previousTotalItemCount = 0;
        loading = true;
    }

    public interface OnBottomReached {
        void loadPage(int page);
    }
}
