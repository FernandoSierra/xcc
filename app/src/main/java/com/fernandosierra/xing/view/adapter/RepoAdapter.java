package com.fernandosierra.xing.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fernandosierra.xing.R;
import com.fernandosierra.xing.data.Repo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since ${DATE}.
 */
public class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {
    private List<Repo> repositories;
    private OnRepoLongClick onRepoLongClick;

    public RepoAdapter() {
        this.repositories = new ArrayList<>();
    }

    public void addRepositories(@NonNull List<Repo> repos) {
        repositories.clear();
        repositories.addAll(repos);
    }

    public void clear() {
        repositories.clear();
    }

    public void setOnRepoLongClick(OnRepoLongClick onRepoLongClick) {
        this.onRepoLongClick = onRepoLongClick;
    }

    @Override
    public RepoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_item, parent, false);
        return new RepoHolder(root);
    }

    @Override
    public void onBindViewHolder(RepoHolder holder, int position) {
        final Repo repo = repositories.get(position);

        holder.name.setText(repo.getName());
        holder.description.setText(repo.getDescription());
        holder.owner.setText(repo.getOwner().getLogin());

        changeItemBackground(holder, repo);

        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (onRepoLongClick != null) {
                    onRepoLongClick.onLongClick(repo);
                }
                return true;
            }
        });
    }

    private void changeItemBackground(RepoHolder holder, Repo repo) {
        int backgroundColor;
        if (repo.isFork()) {
            backgroundColor = R.color.forkedBackground;
        } else {
            backgroundColor = R.color.notForkedBackground;
        }
        holder.root.setBackgroundResource(backgroundColor);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public interface OnRepoLongClick {
        void onLongClick(Repo repo);
    }
}
