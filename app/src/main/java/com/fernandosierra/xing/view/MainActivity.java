package com.fernandosierra.xing.view;

import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.fernandosierra.xing.Constants;
import com.fernandosierra.xing.R;
import com.fernandosierra.xing.data.Repo;
import com.fernandosierra.xing.data.db.RepoDao;
import com.fernandosierra.xing.data.network.NetworkTask;
import com.fernandosierra.xing.data.provider.RepoProvider;
import com.fernandosierra.xing.view.adapter.DividerItemDecorator;
import com.fernandosierra.xing.view.adapter.EndlessScrollListener;
import com.fernandosierra.xing.view.adapter.RepoAdapter;

import java.util.List;

import static com.fernandosierra.xing.Constants.LOADER_ID;

public class MainActivity extends AppCompatActivity implements MainViewInterface, LoaderManager.LoaderCallbacks<Cursor> {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RepoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        downloadPage(INITIAL_PAGE);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    private void initView() {
        swipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipe_layout));
        RecyclerView recyclerView = ((RecyclerView) findViewById(R.id.recycler_view));

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark
        );

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        final EndlessScrollListener endlessScrollListener = new EndlessScrollListener(layoutManager);
        endlessScrollListener.setOnBottomReached(new EndlessScrollListener.OnBottomReached() {
            @Override
            public void loadPage(int page) {
                downloadPage(page);
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecorator(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnScrollListener(endlessScrollListener);
        adapter = new RepoAdapter();
        adapter.setOnRepoLongClick(new RepoAdapter.OnRepoLongClick() {
            @Override
            public void onLongClick(Repo repo) {
                UrlRepoDialog dialog = UrlRepoDialog.newInstance(repo);
                dialog.show(getSupportFragmentManager(), Constants.DIALOG_TAG);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                endlessScrollListener.reset();
                getViewContentResolver().delete(RepoProvider.CONTENT_URI, null, null);
                downloadPage(INITIAL_PAGE);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showRefresh() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void clearUi() {
        adapter.clear();
        adapter.notifyDataSetChanged();
    }

    public void updateUi(@Nullable List<Repo> repositories) {
        if (repositories == null) {
            Toast.makeText(this, R.string.empty_error, Toast.LENGTH_LONG).show();
        } else {
            adapter.addRepositories(repositories);
            adapter.notifyDataSetChanged();
        }
        hideRefresh();
    }

    @Override
    public ContentResolver getViewContentResolver() {
        return getContentResolver();
    }

    private void downloadPage(int page) {
        NetworkTask networkTask = new NetworkTask(this);
        networkTask.execute(page);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                RepoProvider.CONTENT_URI,
                RepoDao.PROJECTION,
                null,
                null,
                RepoDao.ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        data.setNotificationUri(getContentResolver(), RepoProvider.CONTENT_URI);
        updateUi(RepoDao.readRepos(data));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        clearUi();
    }
}
