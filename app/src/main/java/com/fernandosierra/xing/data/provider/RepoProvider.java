package com.fernandosierra.xing.data.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.xing.data.db.DbHelper;
import com.fernandosierra.xing.data.db.RepoDao;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */

public class RepoProvider extends ContentProvider {
    public static final String AUTHORITY = "com.fernandosierra.xing.data.provider";
    public static final int MATCH_CODE = 1;
    private static final String uri = "content://" + AUTHORITY + "/" + RepoDao.TABLE;
    public static final Uri CONTENT_URI = Uri.parse(uri);
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, RepoDao.TABLE, MATCH_CODE);
    }

    private DbHelper helper;

    @Override
    public boolean onCreate() {
        helper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor = null;
        if (uriMatcher.match(uri) == MATCH_CODE) {
            SQLiteDatabase database = helper.getWritableDatabase();
            cursor = database.query(RepoDao.TABLE, projection, selection, selectionArgs, null, null, sortOrder);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Uri resultUri = null;
        if (uriMatcher.match(uri) == MATCH_CODE) {
            SQLiteDatabase database = helper.getWritableDatabase();
            long newId = database.insert(RepoDao.TABLE, null, values);
            resultUri = ContentUris.withAppendedId(CONTENT_URI, newId);
        }
        getContext().getContentResolver().notifyChange(RepoProvider.CONTENT_URI, null);
        return resultUri;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int added = 0;
        if (uriMatcher.match(uri) == MATCH_CODE) {
            SQLiteDatabase database = helper.getWritableDatabase();
            database.beginTransaction();
            for (ContentValues contentValues : values) {
                database.insert(RepoDao.TABLE, null, contentValues);
                added++;
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        }
        getContext().getContentResolver().notifyChange(RepoProvider.CONTENT_URI, null);
        return added;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int changes = 0;
        if (uriMatcher.match(uri) == MATCH_CODE) {
            SQLiteDatabase database = helper.getWritableDatabase();
            changes = database.delete(RepoDao.TABLE, selection, selectionArgs);
        }
        return changes;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int changes = 0;
        if (uriMatcher.match(uri) == MATCH_CODE) {
            SQLiteDatabase database = helper.getWritableDatabase();
            changes = database.update(RepoDao.TABLE, values, selection, selectionArgs);
        }
        getContext().getContentResolver().notifyChange(RepoProvider.CONTENT_URI, null);
        return changes;
    }
}
