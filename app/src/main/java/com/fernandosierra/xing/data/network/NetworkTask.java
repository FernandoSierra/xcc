package com.fernandosierra.xing.data.network;

import android.content.ContentResolver;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fernandosierra.xing.BuildConfig;
import com.fernandosierra.xing.data.Repo;
import com.fernandosierra.xing.data.db.RepoDao;
import com.fernandosierra.xing.data.provider.RepoProvider;
import com.fernandosierra.xing.view.MainViewInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
public class NetworkTask extends AsyncTask<Integer, Void, List<Repo>> {
    private static final String GET = "GET";
    private WeakReference<MainViewInterface> viewInterface;

    public NetworkTask(MainViewInterface viewInterface) {
        this.viewInterface = new WeakReference<>(viewInterface);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        MainViewInterface mainViewInterface = viewInterface.get();
        if (mainViewInterface != null) {
            mainViewInterface.showRefresh();
        }
    }

    @Override
    protected List<Repo> doInBackground(Integer... params) {
        int page = params[0];
        HttpURLConnection connection = null;
        List<Repo> repositories = null;
        String url = String.format(Locale.getDefault(), BuildConfig.BASE_URL, page);
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod(GET);
            connection.connect();
            repositories = parseResponse(connection);
        } catch (IOException e) {
            Log.e(NetworkTask.class.getSimpleName(), e.getMessage(), e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return repositories;
    }

    @Override
    protected void onPostExecute(List<Repo> repos) {
        super.onPostExecute(repos);
        MainViewInterface mainViewInterface = viewInterface.get();
        if (mainViewInterface != null) {
            ContentResolver contentResolver = mainViewInterface.getViewContentResolver();
            contentResolver.bulkInsert(RepoProvider.CONTENT_URI, RepoDao.getContentValues(repos));
        }
    }

    @Nullable
    private List<Repo> parseResponse(@NonNull HttpURLConnection connection) throws IOException {
        List<Repo> repositories = null;
        InputStream inputStream = connection.getInputStream();
        if (inputStream != null) {
            String json = getJsonResponse(inputStream);
            if (json != null) {
                Type listType = new TypeToken<List<Repo>>() {
                }.getType();
                repositories = new Gson().fromJson(json, listType);
            }
        }
        return repositories;
    }

    @Nullable
    private String getJsonResponse(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }
        return new String(builder);
    }
}
