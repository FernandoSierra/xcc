package com.fernandosierra.xing.data.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.fernandosierra.xing.data.Owner;
import com.fernandosierra.xing.data.Repo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */

public class RepoDao implements BaseColumns {
    public static final String TABLE = "Repo";
    public static final String ORDER = _ID + " ASC";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String FORK = "fork";
    private static final String URL = "url";
    private static final String OWNER_ID = "ownerId";
    private static final String OWNER_LOGIN = "ownerLogin";
    private static final String OWNER_URL = "ownerUrl";
    static final String CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
            _ID + " INTEGER PRIMARY KEY NOT NULL," +
            ID + " INTEGER NOT NULL," +
            NAME + " TEXT NOT NULL," +
            DESCRIPTION + " TEXT NOT NULL," +
            FORK + " INTEGER NOT NULL," +
            URL + " TEXT NOT NULL," +
            OWNER_ID + " INTEGER NOT NULL," +
            OWNER_LOGIN + " TEXT NOT NULL," +
            OWNER_URL + " TEXT NOT NULL)";
    public static final String[] PROJECTION = {_ID, ID, NAME, DESCRIPTION, FORK, URL, OWNER_ID, OWNER_LOGIN, OWNER_URL};

    public static List<Repo> readRepos(Cursor cursor) {
        List<Repo> repos = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                repos.add(readRepo(cursor));
            }
        }
        return repos;
    }

    private static ContentValues getContentValue(Repo repo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, repo.getId());
        contentValues.put(NAME, repo.getName());
        contentValues.put(DESCRIPTION, repo.getDescription());
        contentValues.put(FORK, repo.isFork() ? 1 : 0);
        contentValues.put(URL, repo.getHtmlUrl());
        contentValues.put(OWNER_ID, repo.getOwner().getId());
        contentValues.put(OWNER_LOGIN, repo.getOwner().getLogin());
        contentValues.put(OWNER_URL, repo.getOwner().getHtmlUrl());
        return contentValues;
    }

    @NonNull
    private static Repo readRepo(@NonNull Cursor cursor) {
        Repo repo = new Repo();
        repo.setId(cursor.getLong(cursor.getColumnIndexOrThrow(ID)));
        repo.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
        repo.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
        repo.setFork(cursor.getInt(cursor.getColumnIndexOrThrow(FORK)) == 1);
        repo.setHtmlUrl(cursor.getString(cursor.getColumnIndexOrThrow(URL)));
        Owner owner = new Owner();
        owner.setId(cursor.getLong(cursor.getColumnIndexOrThrow(OWNER_ID)));
        owner.setLogin(cursor.getString(cursor.getColumnIndexOrThrow(OWNER_LOGIN)));
        owner.setHtmlUrl(cursor.getString(cursor.getColumnIndexOrThrow(OWNER_URL)));
        repo.setOwner(owner);
        return repo;
    }

    public static ContentValues[] getContentValues(@NonNull List<Repo> repos) {
        int size = repos.size();
        ContentValues[] contentValues = new ContentValues[size];
        for (int index = 0; index < size; index++) {
            contentValues[index] = getContentValue(repos.get(index));
        }
        return contentValues;
    }
}
