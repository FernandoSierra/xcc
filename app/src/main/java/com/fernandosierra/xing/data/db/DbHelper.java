package com.fernandosierra.xing.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    private static final String NAME = "XING.sqlite";
    private static final int VERSION = 1;

    public DbHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(RepoDao.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Nothing
    }
}
