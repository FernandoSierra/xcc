package com.fernandosierra.xing;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 20/01/2017.
 */
public class Constants {
    public static final String EXTRA_REPO = "repo";
    public static final String DIALOG_TAG = "url_repo_dialog";
    public static final int LOADER_ID = 123;

    private Constants() {
        // Nothing
    }
}
